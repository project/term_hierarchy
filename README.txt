You can achieve the terms hierarchey select functionality by using the following stepps.

1- Enable the module 
2- Create the terms if you have not already created.
3- Go to content type *MANAGE FIELDS* Page.
4- Create a new field with *Term reference* and widget *Term Hierarchy*.
5- On field edit page you can see fieldset with name *Term Hierarchy settings
*. 
6- Select the *Select Hierarchy Depth Level* e.g you have three level terms then select the three.

7- Add labels for child terms with comma separate.
